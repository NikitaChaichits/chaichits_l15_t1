package by.nikita.chaichits_l15_t1.ui

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import by.nikita.chaichits_l15_t1.R
import by.nikita.chaichits_l15_t1.SharedPref
import by.nikita.chaichits_l15_t1.adapter.ViewPagerAdapter
import by.nikita.chaichits_l15_t1.utils.AlertDialogUtils
import by.nikita.chaichits_l15_t1.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val utils = Utils()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = ViewPagerAdapter(supportFragmentManager)

        btnTeachMe.setOnClickListener { v ->
            AlertDialogUtils.showAlertDialog(v.context,
                resources.getString(R.string.alertTitle),
                resources.getString(R.string.alertYES),
                resources.getString(R.string.alertNo),
                DialogInterface.OnClickListener { _, _ ->
                    SharedPref.getInstance(v.context).setIsFirstLaunchToTrue()
                    val restart = intent
                    finish()
                    startActivity(restart)
                },
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel()})
        }

        btnGoToArticles.setOnClickListener { v ->
            SharedPref.getInstance(v.context).setIsFirstLaunchToFalse()
            val restart = intent
            finish()
            startActivity(restart)
        }

        if (isFirstLaunch()) {
            tvToolbarTitle.text = resources.getString(R.string.howuse)
            btnTeachMe.visibility = View.INVISIBLE

            utils.setOnBoardingFragments(adapter)

            viewPager.adapter = adapter
            tabsMain.setupWithViewPager(viewPager)

            // keep track of the current screen
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    if (position==2){
                        tabsMain.visibility = View.INVISIBLE
                        btnGoToArticles.visibility = View.VISIBLE
                    }else{
                        btnGoToArticles.visibility = View.INVISIBLE
                        tabsMain.visibility = View.VISIBLE
                    }
                }
            })
        } else {
            tvToolbarTitle.text = resources.getString(R.string.articles)
            btnTeachMe.visibility = View.VISIBLE

            utils.setArticlesFragments(adapter)

            viewPager.adapter = adapter
            tabsMain.setupWithViewPager(viewPager)
        }
    }

    private fun isFirstLaunch(): Boolean {
        return SharedPref.getInstance(applicationContext).isFirstLaunch()
    }
}