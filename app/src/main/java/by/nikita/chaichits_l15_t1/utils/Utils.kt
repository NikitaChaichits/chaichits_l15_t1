package by.nikita.chaichits_l15_t1.utils

import androidx.fragment.app.Fragment
import by.nikita.chaichits_l15_t1.adapter.ViewPagerAdapter
import by.nikita.chaichits_l15_t1.fragments.ArticlesFragment
import by.nikita.chaichits_l15_t1.fragments.OnBoardingFragment
import by.nikita.chaichits_l15_t1.fragments.OnBoardingFragment2
import by.nikita.chaichits_l15_t1.fragments.OnBoardingFragment3

class Utils{
    private val fragments = ArrayList<Fragment>()

    fun setOnBoardingFragments(adapter: ViewPagerAdapter){
        adapter.removeFragments(adapter.fragments)
        fragments.add(OnBoardingFragment())
        fragments.add(OnBoardingFragment2())
        fragments.add(OnBoardingFragment3())
        adapter.addFragments(fragments)
        adapter.notifyDataSetChanged()
    }

    fun setArticlesFragments(adapter: ViewPagerAdapter){
        adapter.removeFragments(adapter.fragments)

        for (i in 1..5) {
            fragments.add(ArticlesFragment())
        }
        adapter.addFragments(fragments)
        adapter.notifyDataSetChanged()
    }
}