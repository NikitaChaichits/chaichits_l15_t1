package by.nikita.chaichits_l15_t1.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

object AlertDialogUtils {

    fun showAlertDialog(
            context: Context,
            title: String,
            positiveButtonText: String,
            negativeButtonText: String,
            onPositiveClicked: DialogInterface.OnClickListener,
            onNegativeClicked: DialogInterface.OnClickListener
    ) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
                .setPositiveButton(positiveButtonText, onPositiveClicked)
                .setNegativeButton(negativeButtonText, onNegativeClicked)
                .setCancelable(false)
                .show()
    }
}