package by.nikita.chaichits_l15_t1.fragments

import android.content.ContentResolver
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import by.nikita.chaichits_l15_t1.R
import kotlinx.android.synthetic.main.fragment_article.*
import kotlin.random.Random

class ArticlesFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_article, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val uri = "android.resource://by.nikita.chaichits_l15_t1/drawable/bb" +
                Random.nextInt(1, 6).toString()
        ivArticleImg.setImageURI(Uri.parse(uri))

        tvArticleText.text = resources.getText(R.string.randomtext).toString() +
                Random.nextInt(1, 11).toString() + "\n" +
                resources.getText(R.string.lorem).toString()
    }
}